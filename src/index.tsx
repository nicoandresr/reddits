import app from 'apprun';
import Main from './containers/main/main';

const root = document.getElementById('root');

new Main().mount(root);

