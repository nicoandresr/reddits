import 'whatwg-fetch';

const URL = 'https://jsonplaceholder.typicode.com/posts/1';

const getCategories = async () => {
  const response = await fetch(URL);
  const data = await response.json();
  return data;
};

export default { getCategories };
