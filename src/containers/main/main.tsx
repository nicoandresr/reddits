import app, { Component } from 'apprun';
import styles from './main.scss';
import redditService from '../../services/reddit';

class Main extends Component {
  state = { greeting: "Hi" };

  view = state => (
    <div className={styles.main}>
      {state}
    </div>
  );

  update = {
    '#': async (state) => {
      const data = await redditService.getCategories();
      return { ...state, data };
    }
  }
}

export default Main;

